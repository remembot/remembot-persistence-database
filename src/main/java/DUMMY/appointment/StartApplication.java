package DUMMY.appointment;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class StartApplication {

	@PostConstruct
  	void started() {
    	TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"));
  	}
  	
    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class, args);
    }
}
