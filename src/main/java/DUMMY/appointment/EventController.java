package DUMMY.appointment;

//import all modules
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/event")
public class EventController {
	
	@Autowired
	private EventRepository eventRepository;
	

	//to get list of allEvents
	@GetMapping("/events")
	public List<Event> getAllEvents() {
		return (List<Event>) eventRepository.findAll();
	}
	
	//to get event in order by Id
	@GetMapping("/events/{id}")
	public ResponseEntity<Event> getEventById(@PathVariable(value = "id") Long eventId) throws ResourceNotFoundException {
		
		Event event = eventRepository.findById(eventId).orElseThrow(() -> new ResourceNotFoundException("Event not found for id: " + eventId));
		
		return ResponseEntity.ok().body(event);
	}
	
	//to save an Event
	@PostMapping("/events")
	public Event createEvent(@Valid @RequestBody Event event) {
		return eventRepository.save(event);
	}


	//to save an Event
	@PutMapping("/events/{id}")
	public ResponseEntity<Event> updateEvent(@PathVariable(value = "id") Long eventId,
			@Valid @RequestBody Event eventDetails) throws ResourceNotFoundException {
		Event event = eventRepository.findById(eventId).orElseThrow(() -> new ResourceNotFoundException("Event not found for id: " + eventId));
		
		event.setName(eventDetails.getName());
		event.setSenderDisplayName(eventDetails.getSenderDisplayName());
		event.setStartDate(eventDetails.getStartDate());
		event.setCreatedDate(eventDetails.getCreatedDate());
		
		Event updatedEvent = eventRepository.save(event);
		return ResponseEntity.ok(updatedEvent);
		
	}
	
	//to delete an Event
	public Map<String, Boolean> deleteEvent(@PathVariable(value = "id") Long eventId) throws ResourceNotFoundException {
		Event event = eventRepository.findById(eventId).orElseThrow(() -> new ResourceNotFoundException("Event not found for id: " + eventId));
		
		eventRepository.delete(event);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
	
	//to get string of created Event
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public void getParameterURLEvent(@RequestParam(required = false) String senderDisplayName, String eventName,
			String startDate) {
      	int year = Integer.parseInt(startDate.substring(0,4)) - 1970;
		int month = Integer.parseInt(startDate.substring(5,7)) - 1;
		int day = Integer.parseInt(startDate.substring(8,10)) - 3;
		int hour = Integer.parseInt(startDate.substring(11,13)) + 2;
		int minute = Integer.parseInt(startDate.substring(14));

		long yearMs = (long) year * 31556952L;
		long monthMs = (long) month * 2629746L;
		long dayMs = (long) day * 86400L;
		long hourMs = (long) hour * 3600L;
		long minuteMs = (long) minute * 60L;
		
		long totalMs = 1000L * (yearMs + monthMs + dayMs + hourMs + minuteMs);
		
		Date createdDate = new Date(totalMs);
		Event newEvent = new Event(eventName, senderDisplayName, createdDate);
		
		eventRepository.save(newEvent);
	}
