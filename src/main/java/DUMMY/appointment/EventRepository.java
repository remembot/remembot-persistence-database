package DUMMY.appointment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

//an interface
public interface EventRepository extends CrudRepository<Event, Long> {

    List<Event> findByName(String name);

}
