package DUMMY.appointment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/event")
public class Dao {

    private static final Logger logger = LoggerFactory.getLogger(Dao.class);

    @PersistenceContext(name = "Entity")
    EntityManager entityManager;


    public void persist(Object entity) {
        entityManager.persist(entity);

    }

    public void remove(Object entity) {
        entityManager.remove(entity);
    }

    public <T> T merge(T entity) {
        return entityManager.merge(entity);
    }

    public Event deleteEvent(Long id) {
        Event e = entityManager.find(Event.class, id);
        entityManager.remove(e);
        return e;
    }


    public List<Event> showEvent(String name) {
        return entityManager
                .createNamedQuery("event.showEvent", Event.class)
                .setParameter("userid", name)
                .getResultList();
    }

    public Event findOldEvent(Long id) {
        return entityManager.find(Event.class, id);
    }

    public Optional<Event> findNextPendingEvent() {
        List<Event> ev = entityManager.createNamedQuery("event.findNextPendingEvent", Event.class)
                .setMaxResults(1)
                .getResultList();
        if (ev != null && !ev.isEmpty()) {
            return Optional.of(ev.get(0));
        }
        return Optional.empty();
    }

    public Optional<Event> findNextEvent() {
        List<Event> ev = entityManager.createNamedQuery("event.findNextEvent", Event.class)
                .setMaxResults(1)
                .getResultList();
        if (ev != null && !ev.isEmpty()) {
            return Optional.of(ev.get(0));
        }
        return Optional.empty();
    }
}
