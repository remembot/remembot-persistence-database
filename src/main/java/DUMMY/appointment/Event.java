package DUMMY.appointment;

//import all modules
import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import java.util.Objects;
import java.util.TimeZone;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

// make an entity with table name event
@Entity
@Table(name = "EVENT")

//query sql that we needed
@NamedQueries({
    @NamedQuery(name = "event.findNextPendingEvent",
            query = "SELECT e from Event e WHERE e.createdDate <= CURRENT_TIMESTAMP order by e.createdDate"),
    @NamedQuery(name = "event.findNextEvent",
            query = "SELECT e from Event e order by e.createdDate"),
    @NamedQuery(name = "event.showEvent",
            query = "SELECT e from Event e where e.senderDisplayName "
                    + "like :userid order by e.createdDate")
})	


public class Event {

    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"));
    }

    //column tuples that we needed for table Event

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "event_id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "sender_display_name", nullable = false)
    private String senderDisplayName;
    
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "start_date")
    private Date startDate;
    
    public Event() {
    }
    //constructor
    public Event(String name) {
    	this.name = name;
        this.createdDate = plus7();
       
    	System.out.println("DATE CREATED: " + this.createdDate);
    }
    
    public Event(String name, String senderDisplayName) {
        this.name = name;
        this.senderDisplayName = senderDisplayName;
        this.createdDate = plus7();
        System.out.println("DATE CREATED: " + this.createdDate);
    }

    public Event(String name, Date startDate, String senderDisplayName) {
        this.name = name;
        this.startDate = startDate;
        this.createdDate = plus7();
        System.out.println("DATE CREATED: " + this.createdDate);
    }
    
    public Event(String name, String senderDisplayName, Date createdDate) {
        this.name = name;
        this.senderDisplayName = senderDisplayName;
        this.createdDate = plus7();
        System.out.println("DATE CREATED: " + this.createdDate);
    }
    
    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    // Setter and Getter
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getCreatedDate() {
        return plus7();
    }

    public Date getStartDate() {
        return this.startDate;
    }
    
    public String getSenderDisplayName() {
    	return this.senderDisplayName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStartDate(Date newStartDate) {
        this.startDate = newStartDate;
    }

    public void setSenderDisplayName(String newSenderDisplayName) {
        this.senderDisplayName = newSenderDisplayName;
    }
    
    public void setCreatedDate(Date newCreatedDate) {
    	this.createdDate = newCreatedDate;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Event event = (Event) o;
        return id == event.id
                && Objects.equals(name, event.name)
                && Objects.equals(createdDate, event.createdDate)
                && Objects.equals(senderDisplayName, event.senderDisplayName);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    //to make GMT+7
    public Date plus7() {
        Date current = this.createdDate;
        Calendar cal = Calendar.getInstance();
        cal.setTime(current);
        cal.add(Calendar.HOUR, +7);
        Date x = cal.getTime();
        return x;
    }
}
