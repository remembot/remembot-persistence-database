package DUMMY.appointment;
import DUMMY.appointment.Event;

import DUMMY.appointment.EventRepository;
import DUMMY.appointment.EventController;
import DUMMY.appointment.EventRepository;
import DUMMY.appointment.StartApplication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=StartApplication.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@SpringBootTest//(classes={EventController.class, Event.class})
public class EventRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EventController controller;

    @Autowired
    private EventRepository repository;

    @Test
    public void contextLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test	
    public void testFindByName() {
        this.entityManager.persist(new Event("kuis statprob"));

        List<Event> events = repository.findByName("kuis statprob");
        
        assertThat(events.get(0).getName()).isEqualTo("kuis statprob");
        assertEquals(1, events.size());
        //assertThat(events).extracting(Event::getName).containsOnly("kuis statprob");

    }

    /*
    @Test
    public void testFindById() {
        Event testEvent = new Event("event name", "sender name");
        entityManager.persist(testEvent);

        Optional<Event> test = repository.findById(testEvent.getId());

        Event fromFindByIdMethod = test.get();
        assertEquals(fromFindByIdMethod.getId(), testEvent.getId());
        assertEquals(fromFindByIdMethod.getName(), testEvent.getName());
        assertEquals(fromFindByIdMethod.getSenderDisplayName(), testEvent.getSenderDisplayName());
    }
    */

}
